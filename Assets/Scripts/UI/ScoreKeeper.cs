using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace AmericanGreetings
{
    public class ScoreKeeper : MonoBehaviour
    {
        [SerializeField] private int tapsToComplete = 15;
        [SerializeField] private TextMeshProUGUI tapCountMessage;
        [SerializeField] private string tapMessage = "Double tap me _ times.";

        [field: SerializeField] public UnityEvent OnTapOut { get; set; } = new UnityEvent();

        private int taps = 0;

        private void Start()
        {
            UpdateTapCountMessage();
        }

        public void Tap()
        {
            if (taps >= tapsToComplete - 1)
            {
                tapCountMessage.text = "Tapped out.";
                this.enabled = false;
                OnTapOut.Invoke();
            }
            else
            {
                taps++;
                UpdateTapCountMessage();
            }
        }

        private void UpdateTapCountMessage()
        {
            tapCountMessage.text = tapMessage.Replace("_", (tapsToComplete-taps).ToString());
        }
    }
}