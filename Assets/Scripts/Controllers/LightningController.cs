using System.Collections;
using UnityEngine;

namespace AmericanGreetings.Controllers
{
    /// <summary>
    /// Can be used to produce a visual effect imitating lightning
    /// and also an audio effect imitating thunder.
    /// Intended to be used by the <see cref="Director"/> in the "Main" scene.
    /// </summary>
    public class LightningController : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer sprite;
        [SerializeField] private AudioSource asrc;
        [SerializeField] private AudioClip[] thunderStrikes;
        [SerializeField] private float thunderDuration = 0.4f;
        [SerializeField] private float thunderDelay = 0.4f;

        /// <summary>
        /// Public method used to "instantiate" a thunder+lightning strike.
        /// </summary>
        public void Flash()
        {
            StartCoroutine(IEThunderstrike());
        }

        /// <summary>
        /// Effectively an animation for that produces the effect of lightning and thunder.
        /// </summary>
        /// <returns></returns>
        private IEnumerator IEThunderstrike()
        {
            //lightning
            float t = 1;

            var white = Color.white;
            var clear = Color.clear;

            sprite.color = Color.white;
            while (t > 0)
            {
                sprite.color = Color.Lerp(clear, white, t);
                t -= Time.deltaTime / thunderDuration;
                yield return null;
            }
            sprite.color = Color.clear;

            //thunder sounds some time after lightning
            yield return new WaitForSeconds(thunderDelay);
            asrc.PlayOneShot(thunderStrikes[Random.Range(0, thunderStrikes.Length)]);
        }
    }
}
