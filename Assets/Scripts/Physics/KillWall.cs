using UnityEngine;

namespace AmericanGreetings
{
    /// <summary>
    /// Will destroy any <see cref="Rigidbody2D"/> that enters it or collides with it.
    /// </summary>
    public class KillWall : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            Destroy(collision.gameObject);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Destroy(collision.otherCollider.gameObject);
        }
    }
}