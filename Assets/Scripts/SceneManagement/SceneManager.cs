using UnityEngine;

namespace AmericanGreetings.Scenes
{
    /// <summary>
    /// Provides a set of useful functions relating to <see cref="UnityEngine.SceneManagement"/>.
    /// </summary>
    public class SceneManager : MonoBehaviour
    {
        /// <summary>
        /// Asynchronously loads the next scene.
        /// </summary>
        public void NextScene()
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}