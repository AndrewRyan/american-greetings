Shader "Custom/Stencil"
{
	SubShader
	{
		Zwrite off
		colorMask 0
		Cull off

		Stencil
		{
			Ref 1
			Comp always
			Pass replace
		}

		Pass
		{
		}
	}
}