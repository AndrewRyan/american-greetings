using UnityEngine;
using UnityEngine.Events;

namespace AmericanGreetings.Controllers
{
    /// <summary>
    /// Enables a sprite (Hexagon) to be double-tapped.
    /// Input is cross-platform compatible.
    /// </summary>
    public class HexagonController : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private ShapeManager shapeManager;

#if UNITY_STANDALONE || UNITY_EDITOR
        private bool isMouseOver = false;
#endif
        private PolygonCollider2D collision;
        private bool coolingDown = false;

        /// <summary>
        /// An inspector-serialized event that can be used to trigger other events, like the <see cref="SceneManager"/>.
        /// </summary>
        [field: SerializeField] public UnityEvent OnHexDoubleClick { get; set; } = new UnityEvent();

        private void Awake()
        {
            collision = GetComponent<PolygonCollider2D>();
            shapeManager.OnShapeSpriteChange.AddListener(OnShapeChange);
            SetRandomColor();
        }

        private void OnDestroy()
        {
            shapeManager.OnShapeSpriteChange.RemoveListener(OnShapeChange);
        }

        /// <summary>
        /// Sets the color of our hexagon's sprite renderer to a randomly selected one.
        /// </summary>
        private void SetRandomColor()
        {
            spriteRenderer.color = GetRandomColor();
        }

        private void OnDoubleTap()
        {
            OnHexDoubleClick.Invoke();
            SetRandomColor();
        }

        public void OnShapeChange(Sprite sprite)
        {
            spriteRenderer.sprite = sprite;

            if (collision != null)
                Destroy(collision);

            collision = this.gameObject.AddComponent<PolygonCollider2D>();
        }

        /// <summary>
        /// Returns a random color.
        /// </summary>
        /// <returns></returns>
        private Color GetRandomColor()
        {
            return Random.ColorHSV();
        }

#if UNITY_STANDALONE || UNITY_EDITOR
        private void OnMouseEnter()
        {
            isMouseOver = true;
        }

        private void OnMouseExit()
        {
            isMouseOver = false;
        }

        private void OnGUI()
        {
            if (!isMouseOver)
                return;

            var e = Event.current;
            if (e.isMouse)
            {
                if (e.clickCount == 2)
                {
                    OnDoubleTap();
                }
            }
        }

#elif UNITY_ANDROID || UNITY_IOS
        private void Update()
        {
            if (Input.touchCount > 0)
            {
                if (coolingDown)
                {
                    return;
                }
                else
                {
                    if (Input.touchCount == 1)
                    {
                        var touch = Input.GetTouch(0);
                        if (touch.tapCount == 2)
                        {
                            var wp = Camera.main.ScreenToWorldPoint(touch.position);
                            var touchPos = new Vector2(wp.x, wp.y);
                            if (collision == Physics2D.OverlapPoint(touchPos))
                            {
                                OnDoubleTap();
                                coolingDown = true;
                            }
                        }
                    }
                }
            }
            else
            {
                coolingDown = false;
            }
        }
#endif
    }
}