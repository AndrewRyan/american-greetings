using UnityEngine;

namespace AmericanGreetings
{
    /// <summary>
    /// Enables the spawning of a box from prefab (<see cref="boxPrefab"/>) or scene reference at a given location <see cref="spawnLocation"/>.
    /// </summary>
    public class BoxSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject boxPrefab;
        [SerializeField] private Transform spawnLocation;

        /// <summary>
        /// Attempts to spawn a box.
        /// </summary>
        public void SpawnBox()
        {
            Instantiate(boxPrefab, spawnLocation.position, Quaternion.identity);
        }
    }
}