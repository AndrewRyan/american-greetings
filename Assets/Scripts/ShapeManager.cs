using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;
using UnityEngine.Events;

namespace AmericanGreetings
{
    /// <summary>
    /// Retains the state of the current <see cref="EShapes"/> shape.
    /// Broadcasts some events each time the <see cref="currentShape"/> has changed.
    /// </summary>
    public class ShapeManager : MonoBehaviour
    {
        #region member variables
        public enum EShapes
        {
            Triangle,
            Box,
            Pentagon,
            Hexagon,
            Circle,
        }

        private EShapes currentShape = EShapes.Hexagon;

        [System.Serializable] private class ShapeMap : SerializableDictionaryBase<EShapes, Sprite> { }
        [SerializeField] private ShapeMap shapeMap = new ShapeMap();
        
        [System.Serializable] public class ShapeEvent : UnityEvent<EShapes> { }
        [field: SerializeField] public ShapeEvent OnShapeChangeEvent { get; set; } = new ShapeEvent();

        [System.Serializable] public class ShapeEventImage : UnityEvent<Sprite> { }
        [field: SerializeField] public ShapeEventImage OnShapeSpriteChange { get; set; } = new ShapeEventImage();
        #endregion

        #region unity events
        private void Start()
        {
            SetShape(currentShape);
        }
        #endregion

        #region public methods
        /// <summary>
        /// Manually set the shape and broadcast events.
        /// </summary>
        /// <param name="shape"></param>
        public void SetShape(EShapes shape)
        {
            currentShape = shape;
            BroadcastState();
        }

        public void IncrementShape()
        {
            //can we increment current shape?
            if((int)currentShape < System.Enum.GetValues(typeof(EShapes)).Length -1)
            {
                currentShape++;
            }
            //roll back to 0 so as not to exceed the bounds of our enum
            else
            {
                currentShape = 0;
            }

            //broadcast some events
            BroadcastState();
        }
        #endregion

        #region private methods
        /// <summary>
        /// Broadcasts events that indicate our state.
        /// </summary>
        private void BroadcastState()
        {
            OnShapeChangeEvent.Invoke(currentShape);
            OnShapeSpriteChange.Invoke(shapeMap[currentShape]);
        }
        #endregion

    }
}