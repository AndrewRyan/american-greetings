using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace AmericanGreetings.Controllers
{
    /// <summary>
    /// Used to create a black screen that can be faded in or out for cinematic effect.
    /// Intended to be located on the main canvas, and the <see cref="blackSprite"/> over all other
    /// sprites in the canvas.
    /// </summary>
    public class MaskController : MonoBehaviour
    {
        [SerializeField] private Image blackSprite;

        /// <summary>
        /// Fades the <see cref="blackSprite"/> color from black to clear.
        /// </summary>
        /// <param name="duration"></param>
        public void FadeIn(float duration)
        {
            StartCoroutine(IEFadeIn(duration));
        }

        /// <summary>
        /// Fades the <see cref="blackSprite"/> color from clear to black.
        /// </summary>
        /// <param name="duration"></param>
        public void FadeOut(float duration)
        {
            StartCoroutine(IEFadeOut(duration));
        }

        /// <summary>
        /// The "animation" that effectively fades from black to clear.
        /// </summary>
        /// <param name="duration"></param>
        /// <returns></returns>
        private IEnumerator IEFadeIn(float duration)
        {
            float t = 0;
            while (t < 1)
            {
                blackSprite.color = Color.Lerp(Color.black, Color.clear, t);
                t += Time.deltaTime / duration;
                yield return null;
            }
        }

        /// <summary>
        /// The "animation" that effectively fades from clear to black.
        /// </summary>
        /// <param name="duration"></param>
        /// <returns></returns>
        private IEnumerator IEFadeOut(float duration)
        {
            float t = 1;
            while (t > 0)
            {
                blackSprite.color = Color.Lerp(Color.black, Color.clear, t);
                t -= Time.deltaTime / duration;
                yield return null;
            }
            blackSprite.color = Color.black;
        }
    }
}